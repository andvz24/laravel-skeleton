<?php

namespace App\Http\Controllers\Admin\Page;

use App\Http\Requests\PageRequest;
use App\Models\Pages\Page;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PageController extends Controller
{
    public function index()
    {
        $pages = Page::defaultOrder()->get()->toTree();
        return view('admin.page.list', compact('pages'));
    }

    public function show($id)
    {
        return view('admin.page.view', [
            'pages' => Page::get()->toFlatTree(),
            'page' => Page::find($id)
        ]);
    }

    public function create()
    {
        return view('admin.page.view', [
            'pages' => Page::get()->toFlatTree(),
            'page' => new Page()
        ]);
    }

    public function store(PageRequest $request)
    {
        $validation = $request->validated();
        $page = Page::create($validation);
        $page->parent_id = (int) $request->get('parent_id');
        $page->save();

        return redirect('admin/pages/' . $page->id)->with('success', 'Страница успешно сохранена!');
    }

    public function update(PageRequest $request, $id)
    {
        $validation = $request->validated();
        $page = Page::find($id);
        $page->update($validation);
        $page->parent_id = (int) $request->get('parent_id');
        $page->save();

        return redirect('admin/pages/' . $page->id)->with('success', 'Страница успешно изменен!');
    }

    public function destroy($id)
    {
        $page = Page::find($id);
        $page->delete();

        return redirect('admin/pages')->with('success', 'Страница удалена');
    }

    public function processDragAndDrop(Request $request): RedirectResponse
    {
        $pageId = (int) $request->get('id');
        $page = Page::find($pageId);
        if (!$page) {
            return back()->with('error', 'Page not found');
        }

        $parentId = (int) $request->get('parent');

        if ($parentId) {
            $pageParent = Page::find($parentId);
        }else{
            $pageParent = Page::find(1);
        }
        if (!$pageParent) {
            return back()->with('error', 'Parent page not found');
        }

        $page->parent_id = $pageParent->id;
        $page->save();

        $position = (int) $request->get('position'); // 2
        $oldPosition = (int) $request->get('old_position'); // 0

        if ($position > $oldPosition) {
            $page->down($oldPosition + $position);
        }else{
            $page->up($oldPosition + $position);
        }

        return back()->with('success', 'OK');
    }
}
