<?php

namespace App\Http\Controllers\Admin\News;

use App\Http\Requests\NewsRequest;
use App\Http\Controllers\Controller;
use App\Models\News\News;

class NewsController extends Controller
{
    public function index()
    {
        return view('admin.news.list', [
            'news' => News::orderBy('created_at','desc')->paginate(10)
        ]);
    }

    public function show($id)
    {
        return view('admin.news.view', [
            'news' => News::find($id)
        ]);
    }

    public function create()
    {
        return view('admin.news.view', [
            'news' => new News()
        ]);
    }

    public function store(NewsRequest $request)
    {
        $validation = $request->validated();
        $news = News::create($validation);

        return redirect('admin/news/' . $news->id)->with('success', 'Новость успешно сохранен!');
    }

    public function update(NewsRequest $request, int $id)
    {
        $validation = $request->validated();
        $news = News::find($id);
        $news->update($validation);

        return redirect('admin/news/' . $news->id)->with('success', 'Новость успешно изменена!');
    }

    public function destroy(int $id)
    {
        $news = News::find($id);
        $news->delete();

        return redirect('admin/news')->with('success', 'Новость удалена');
    }
}
