<?php

namespace App\Http\Controllers\Admin\Setting;


use App\Models\Settings\Settings;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;

class SettingController extends Controller
{

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Settings  $settings
     * @return Application|Factory|View|Response
     */
    public function edit(Settings $settings)
    {
        return view('admin.setting.view', [
            'settings' => Settings::find(1)
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @return Application|RedirectResponse|Response|Redirector
     */
    public function update(Request $request, $id)
    {
        $setting = Settings::find($id);
        $setting->update($request->post());
        return redirect('admin/settings')->with('success', 'Настройки успешно сохранены!');
    }
}
