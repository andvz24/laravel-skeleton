<?php

namespace App\Http\Controllers\Admin\Feedback;

use App\Http\Requests\FeedbackRequest;
use App\Models\Feedbacks\Feedbacks;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FeedbackController extends Controller
{

    public function index()
    {
        return view('admin.feedback.list', [
            'feedbacks' => Feedbacks::orderBy('created_at','desc')->paginate(10)
        ]);
    }

    public function show($id)
    {
        return view('admin.feedback.view', [
            'feedback' => Feedbacks::find($id)
        ]);
    }

    public function create()
    {
        return view('admin.feedback.view', [
            'feedback' => new Feedbacks()
        ]);
    }

    public function store(FeedbackRequest $request)
    {
        $validation = $request->validated();
        $feedback = Feedbacks::create($validation);

        return redirect('admin/feedbacks/' . $feedback->id)->with('success', 'Обращение успешно сохранен!');
    }

    public function update(FeedbackRequest $request, int $id)
    {
        $validation = $request->validated();
        $feedback = Feedbacks::find($id);
        $feedback->update($validation);

        return redirect('admin/feedbacks/' . $feedback->id)->with('success', 'Обращение успешно изменена!');
    }

    public function destroy(int $id)
    {
        $feedback = Feedbacks::find($id);
        $feedback->delete();

        return redirect('admin/feedbacks')->with('success', 'Обращение удалена');
    }
}
