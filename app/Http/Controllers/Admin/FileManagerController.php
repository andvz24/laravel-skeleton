<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

class FileManagerController extends Controller
{
    public function show()
    {
        return view('admin.file-manager');
    }
}
