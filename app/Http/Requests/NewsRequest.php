<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class NewsRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        $id = (int) $this->route()->originalParameters();
        return [
            'title' => 'required|max:255',
            'alias' => 'required|unique:news,alias, ' . (isset($id)  ? ',' . $id : ' ').'|max:255',
            'date' => 'required|date',
            'image' => 'string|nullable',
        ];
    }

    public function messages(): array
    {
        return [
            'title.required' => 'Укажите заголовок!',
            'title.max' => 'Заголовок слишком большой!',
            'alias.required' => 'Укажите алиас!',
            'alias.max' => 'Алиас слишком большой!',
            'alias.unique' => 'Алиас должен быть уникальным!',
        ];
    }
}
