<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PageRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'title' => 'required|max:255',
            'alias' => 'required|unique:news|max:255',
            'content' => 'nullable',
            'parent_id' => 'integer|min:0',
        ];
    }

    public function messages(): array
    {
        return [
            'title.required' => 'Укажите заголовок!',
            'title.max' => 'Заголовок слишком большой!',
            'alias.required' => 'Укажите алиас!',
            'alias.max' => 'Алиас слишком большой!',
            'alias.unique' => 'Алиас должен быть уникальным!',
        ];
    }
}
