<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class FeedbackRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'name' => 'required|max:255',
            'email' => 'required|max:40',
            'phone' => 'required|max:17',
            'question' => 'required',
        ];
    }

    public function messages(): array
    {
        return [
            'name.required' => 'Заполните ФИО!',
            'name.max' => 'ФИО слишком длинное!',
        ];
    }
}
