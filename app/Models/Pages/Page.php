<?php

namespace App\Models\Pages;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Kalnoy\Nestedset\NodeTrait;

class Page extends Model
{
    use HasFactory;
    use NodeTrait;

    protected $table = 'pages';

    protected $fillable = [
        'title',
        'alias',
        'parent_id',
        'content'
    ];
}
