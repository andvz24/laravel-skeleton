<?php

namespace App\Models\News;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    use HasFactory;

    protected $table = 'news';

    protected $fillable = [
        'title',
        'alias',
        'date',
        'anonce',
        'content',
        'image',
        'is_published'
    ];

    protected $casts = [
        'date' => 'datetime',
    ];

}
