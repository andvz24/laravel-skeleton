$(function () {

    var sortableObject = $("#js-sortable");

    sortableObject.sortable({
        items: "tr:not(.js-ui-sort-disabled)",
        delay: 200,
        axis: "y",
        update: function(event, ui) {

            var url = ui.item.attr('data-sortable-url');
            var new_position = parseInt(ui.item.index()) - 1;
            $.post(url, {position: new_position});
        }
    });

});
