var input = '';

$('.js-open-elfinder-multiple').on('click', function () {
    input = $(this).closest('.input-group').find('input');
    window.open('/js/app/libs/elfinder/elfinder-modal-multiple.html', 'Файлы',
        'status=0, toolbar=0, location=0, menubar=0, directories=0, ' +
        'resizable=1, scrollbars=0, width=800, height=600'
    );
});

function setFilePathValue(file)
{
    input.val(file);
}
