function transliterate(text) {
    var en_to_ru = {
        'а': 'a',
        'б': 'b',
        'в': 'v',
        'г': 'g',
        'д': 'd',
        'е': 'e',
        'ё': 'jo',
        'ж': 'zh',
        'з': 'z',
        'и': 'i',
        'й': 'j',
        'к': 'k',
        'л': 'l',
        'м': 'm',
        'н': 'n',
        'о': 'o',
        'п': 'p',
        'р': 'r',
        'с': 's',
        'т': 't',
        'у': 'u',
        'ф': 'f',
        'х': 'h',
        'ц': 'c',
        'ч': 'ch',
        'ш': 'sh',
        'щ': 'sch',
        'ъ': '',
        'ы': 'y',
        'ь': '',
        'э': 'e',
        'ю': 'ju',
        'я': 'ja',
        ' ': '-',
        'і': 'i',
        'ї': 'i',
        'є': 'e',
        'А': 'A',
        'Б': 'B',
        'В': 'V',
        'Г': 'G',
        'Д': 'D',
        'Е': 'E',
        'Ё': 'Jo',
        'Ж': 'Zh',
        'З': 'Z',
        'И': 'I',
        'Й': 'J',
        'К': 'K',
        'Л': 'L',
        'М': 'M',
        'Н': 'N',
        'О': 'O',
        'П': 'P',
        'Р': 'R',
        'С': 'S',
        'Т': 'T',
        'У': 'U',
        'Ф': 'F',
        'Х': 'H',
        'Ц': 'C',
        'Ч': 'Ch',
        'Ш': 'Sh',
        'Щ': 'Sch',
        'Ъ': '',
        'Ы': 'Y',
        'Ь': '',
        'Э': 'E',
        'Ю': 'Ju',
        'Я': 'Ja',
        'І': 'I',
        'Ї': 'I',
        'Є': 'E'
    };


    text = trans_trim(text);
    text = text.split("");

    var trans = '';

    for (i = 0; i < text.length; i++) {
        for (var key in en_to_ru) {
            var val = en_to_ru[key];
            if (key == text[i]) {
                trans += val;
                break;
            }
        }
    }

    return trans.toLowerCase();
}

function trans_trim(string) {
    string = string.replace(/(^\s+)|'|"|<|>|\!|\||@|#|$|%|^|\^|\$|\\|\/|&|\*|\(|\)|\|\/|;|\+|№|,|\?|:|{|}|\[|\]/g, "");
    return string;
}

$(function () {
    $('#js-transliterate-run').on('click', function (e) {
        e.preventDefault();
        var input_destination = $('input.js-transliterate-destination');
        var source = $('input.js-transliterate-source').val();
        input_destination.val(transliterate(source));
    });
});
