$(function () {
    CKEDITOR.replaceClass = 'ckeditor';
    CKEDITOR.config.extraAllowedContent = 'a span';
    // CKEDITOR.config.enterMode = CKEDITOR.ENTER_BR;
    CKEDITOR.config.shiftEnterMode = CKEDITOR.ENTER_P;
    CKEDITOR.dtd.$removeEmpty.span = false;
    CKEDITOR.dtd.$removeEmpty.i = false;
    CKEDITOR.config.allowedContent = true;
    CKEDITOR.config.extraPlugins = 'youtube, html5video';
    CKEDITOR.config.language = 'ru';
    CKEDITOR.config.filebrowserBrowseUrl = '/assets/cms-ks/app/libs/elfinder/elfinder-cke.html';
    CKEDITOR.config.toolbarGroups = [
        { name: 'document', groups: [ 'mode', 'document', 'doctools' ] },
        { name: 'clipboard', groups: [ 'clipboard', 'undo' ] },
        { name: 'editing', groups: [ 'find', 'selection', 'spellchecker', 'editing' ] },
        { name: 'forms', groups: [ 'forms' ] },
        '/',
        { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
        { name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi', 'paragraph' ] },
        { name: 'links', groups: [ 'links' ] },
        { name: 'insert', groups: [ 'insert' ] },
        { name: 'styles', groups: [ 'styles' ] },
        { name: 'colors', groups: [ 'colors' ] },
        { name: 'tools', groups: [ 'tools' ] },
        { name: 'others', groups: [ 'others' ] },
        { name: 'about', groups: [ 'about' ] }
        ];

    CKEDITOR.config.removeButtons = 'Styles,Font,FontSize,Form,Checkbox,Radio,TextField,Textarea,Select,Button,ImageButton,HiddenField,Flash,Smiley';
});
