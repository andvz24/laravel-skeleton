$(function () {
    $(".select2").select2({
        templateSelection: function (value) {
            return value.text.replace(/^[\s-]+/g, '');
        }
    });
});
