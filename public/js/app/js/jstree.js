$(function () {
    var idJsTree = '#js-tree';
    $(idJsTree).jstree({
        "core" : {
            "multiple" : false,
            "check_callback" : true,
            "themes" : {
                "dots" : false,
                "variant" : "large",
                "responsive": true,
                "stripes": true,
                "icons": false,
            }
        },
        "state": {
            "events": "activate_node.jstree",
            "key" : $(idJsTree).attr('data-tree-key')
        },
        "plugins" : ["types", "wholerow", "state", "dnd"]
    }).bind("activate_node.jstree", function (e, data) {
        var url = data.node.a_attr.href;
        setTimeout(function () { location.href = url; }, 100);

    }).bind("move_node.jstree", function (e, data) {
        var url = $(idJsTree).attr('data-move-url');
        var token = $(idJsTree).attr('data-token');
        var postData = {id: data.node.id, parent: data.parent, position: data.position, old_position: data.old_position, _token: token};
        console.log(data);
        $.post(url, postData);
    });
});
