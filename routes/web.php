<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['prefix' => 'admin', 'namespace' => 'App\\Http\\Controllers\\Admin', 'middleware' => 'auth'], function() {
    Route::get('/', 'MainController@index')->name('admin.index');
    Route::get('/file-manager', 'FileManagerController@show')->name('admin.file-manager');
    Route::get('/settings', 'Setting\SettingController@edit')->name('admin.settings');
    Route::post('/settings/update/{id}', 'Setting\SettingController@update')->name('settings.update');
    Route::resource('news', 'News\NewsController');
    Route::resource('pages', 'Page\PageController');
    Route::resource('feedbacks', 'Feedback\FeedbackController');
    Route::post('/pages/drag-and-drop', 'Page\PageController@processDragAndDrop')->name('pages.processDragAndDrop');
});

Auth::routes();


