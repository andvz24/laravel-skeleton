<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <title>Панель администрирования</title>
    <link rel="stylesheet" href="/vendor/fontawesome-free/css/all.min.css">
    <link rel="stylesheet" href="/vendor/adminlte/dist/css/adminlte.min.css">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">

    <nav class="main-header navbar navbar-expand navbar-white navbar-light">
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
            </li>
        </ul>

        <ul class="navbar-nav ml-auto">
            <li class="nav-item">
                <a class="nav-link" data-widget="control-sidebar" data-slide="true" href="#" role="button">
                    <i class="fas fa-th-large"></i>
                </a>
            </li>
        </ul>
    </nav>

    <aside class="main-sidebar sidebar-dark-primary elevation-4">
        <a href="/admin" class="brand-link">
            <img src="/vendor/adminlte/dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
                 style="opacity: .8">
            <span class="brand-text font-weight-light">Админка</span>
        </a>

        <div class="sidebar">
            <nav class="mt-2">
                <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                    <li class="nav-item">
                        <a href="{{ route('news.index') }}" class="nav-link @if (request()->route()->getName() === 'news.index')active @endif">
                            <i class="nav-icon far fa-file-alt"></i>
                            <p>Новости</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('pages.index') }}" class="nav-link @if (request()->route()->getName() === 'pages.index')active @endif">
                            <i class="nav-icon far fa-copy"></i>
                            <p>Страницы и меню</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('feedbacks.index') }}" class="nav-link @if (request()->route()->getName() === 'feedbacks.index')active @endif">
                            <i class="nav-icon far fa-comment-dots"></i>
                            <p>Обратная связь</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('admin.file-manager') }}" class="nav-link @if (request()->route()->getName() === 'admin.file-manager')active @endif">
                            <i class="nav-icon far fa-folder-open"></i>
                            <p>Файлы</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('admin.settings') }}" class="nav-link @if (request()->route()->getName() === 'admin.settings')active @endif">
                            <i class="nav-icon fa fa-cogs"></i>
                            <p>Настройки</p>
                        </a>
                    </li>
{{--                    <li class="nav-item">--}}
{{--                        <a href="{{ route('admin.users') }}" class="nav-link @if (request()->route()->getName() === 'admin.users')active @endif">--}}
{{--                            <i class="nav-icon far fa-folder-open"></i>--}}
{{--                            <p>Пользователи</p>--}}
{{--                        </a>--}}
{{--                    </li>--}}
{{--                    <li class="nav-item">--}}
{{--                        <a href="{{ route('admin.file-manager') }}" class="nav-link @if (request()->route()->getName() === 'admin.file-manager')active @endif">--}}
{{--                            <i class="nav-icon far fa-folder-open"></i>--}}
{{--                            <p>Выход</p>--}}
{{--                        </a>--}}
{{--                    </li>--}}
                </ul>
            </nav>
        </div>
    </aside>

    <div class="content-wrapper">
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">@yield('title', 'Панель администирования')</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{ route('admin.index') }}">Главня</a></li>
                            @yield('breadcrumbs')
                        </ol>
                    </div>
                </div>
            </div>
        </div>

        <div class="content">
            <div class="row">
                @yield('content')
            </div>
        </div>

    </div>

    <aside class="control-sidebar control-sidebar-dark">
        <div class="p-3">
            <h5>Title</h5>
            <p>Sidebar content</p>
        </div>
    </aside>

    <footer class="main-footer">
        <strong>&copy; 2011-{{ date('Y') }} <a href="https://www.15web.ru/" target="_blank">Студия 15</a>.</strong>
    </footer>
</div>

@yield('css')

@section('js')
    <script src="/vendor/jquery/jquery.min.js"></script>
    <script src="/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="/vendor/adminlte/dist/js/adminlte.min.js"></script>
@show

</body>
</html>
