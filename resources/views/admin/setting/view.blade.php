@extends('admin.layout.form')

@section('title', 'Настройки')

@section('breadcrumbs')
    <li class="breadcrumb-item">Настройки</li>
@endsection

@section('form')
    @if (Session::has('success'))
        <div class="alert alert-success" role="alert">
            {{ Session::get('success') }}
        </div>
    @endif
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <form role="form"
          action="{{ route('settings.update', $settings->id) }}"
          method="POST">
        @csrf
        <div class="card-body">
            <div class="form-group">
                <label for="sitename">Название сайта</label>
                <input type="text" class="form-control" id="sitename" name="sitename" placeholder="Название сайта" value="{{ $settings->sitename }}">
            </div>
            <div class="form-group">
                <label for="email">Почта</label>
                <input type="text" class="form-control" id="email" name="email" placeholder="Почта" value="{{ $settings->email }}">
            </div>
            <div class="form-group">
                <label for="feedback_email">Почта для получения писем</label>
                <input type="text" class="form-control" id="feedback_email" name="feedback_email" placeholder="Почта для получения писем" value="{{ $settings->feedback_email }}">
            </div>
        </div>

        <div class="card-footer">
            <button type="submit" class="btn btn-success">Сохранить</button>
        </div>
    </form>
@stop
