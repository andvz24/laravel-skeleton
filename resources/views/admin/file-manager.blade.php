@extends('admin.layout')

@section('title', 'Файлы')

@section('breadcrumbs')
    <li class="breadcrumb-item">Файлы</li>
@endsection

@section('content_header')
    <h1>Файлы</h1>
@stop

@section('content')
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-body">
                <iframe frameborder="0" id="fm" src="{{ asset('js/app/libs/elfinder/elfinder.html') }}" width="100%" height="450"></iframe>
            </div>
        </div>
    </div>
@stop

@section('css')

@stop

@section('js')

@stop
