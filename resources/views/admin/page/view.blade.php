@extends('admin.layout.form')

@section('title')
    Редактировать страницу
@stop

@section('breadcrumbs')
    <li class="breadcrumb-item"><a href="{{ route('pages.index') }}">Новости</a></li>
    <li class="breadcrumb-item">Создание новости</li>
@endsection

@section('form')
    @if (Session::has('success'))
        <div class="alert alert-success" role="alert">
            {{ Session::get('success') }}
        </div>
    @endif
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <form role="form"
          action="@if($page->id){{ route('pages.update', $page->id) }}@else{{ route('pages.store') }}@endif"
          method="POST">
        @if($page->id)
            @method('put')
        @endif
        @csrf
        <div class="card-body">
            <div class="form-group">
                <label for="title">Заголовок</label>
                <input type="text" class="form-control js-transliterate-source" id="title" name="title" placeholder="Заголовок" value="{{ $page->title }}">
            </div>
            <div class="form-group">
                <label for="alias">Алиас</label>
                <a href="#" id="js-transliterate-run" class="float-right" title="Сгенерировать алиас">Сгенерировать</a>
                <input type="text" class="form-control js-transliterate-destination" id="alias" name="alias" placeholder="Алиас" value="{{ $page->alias }}">
            </div>
            <div class="form-group">
                <label for="parent">Родительская страница</label>
                <select class="form-control" name="parent_id">
                    @foreach($pages as $item)
                        @if($page->id !== $item->id)
                            <option value="{{ $item->id }}" @if($page->parent_id === $item->id) selected @endif> {{ $item->title }}</option>
                        @endif
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="content">Контент</label>
                <textarea name="content" id="content" class="ckeditor form-control">{{ $page->content }}</textarea>
            </div>

            <div class="form-group">
                <label class="control-label required">Опубликовано</label>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="is_published" value="1" @if($page->is_published === 1) checked @endif>
                    <label class="form-check-label">Да</label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="is_published" value="0" @if($page->is_published  === 0) checked @endif>
                    <label class="form-check-label">Нет</label>
                </div>
            </div>
        </div>

        <div class="card-footer">
            <button type="submit" class="btn btn-sm btn-success">Сохранить</button>
            @if($page->id)
                <a href="{{ route('pages.create') }}" type="button" class="btn btn-sm btn-primary">Создать</a>
                <a href="{{ route('pages.destroy', $page->id) }}" type="button" class="btn btn-sm btn-danger float-right">Удалить</a>
            @endif
        </div>
    </form>
    @if($page->id)
        <form class="form" method="POST" action="{{ route('pages.destroy', $page->id) }}">
            @csrf
            @method('DELETE')
            <div class="card-body">
                <button type="submit" class="btn btn-sm btn-danger float-right">Удалить</button>
            </div>
        </form>
    @endif
@stop

@section('css')
    @parent
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker3.min.css">
@stop

@section('js')
    @parent

    <script src="{{ asset('js/app/libs/ckeditor/ckeditor.js') }}"></script>
    <script src="{{ asset('js/app/js/ckeditor.js') }}"></script>
    <script src="{{ asset('js/app/js/elfinder-file-type.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/locales/bootstrap-datepicker.ru.min.js"></script>
    <script src="{{ asset('js/app/js/datapicker.js') }}"></script>
    <script src="{{ asset('js/app/js/transliterate.js') }}"></script>
@stop


