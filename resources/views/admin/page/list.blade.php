@extends('admin.layout.table')

@section('title', 'Страницы и меню')

@section('breadcrumbs')
    <li class="breadcrumb-item">Структура сайта</li>
@endsection

@section('create_url')
    {{ route('pages.create') }}
@endsection

@section('table')
    <div id="js-tree" class="p-2" data-move-url="{{ route('pages.processDragAndDrop') }}" data-token="{{ csrf_token() }}" data-tree-key="page">
        <ul role="group">
            @foreach($pages as $item)
                <li role="treeitem" class="jstree-open" aria-level="1" id="{{ $item->id }}">{{ $item->title }}
                    @if($item->children)
                        <ul role="group" class="jstree-children">
                            @foreach($item->children as $cats)
                                <li role="treeitem" aria-level="2" id="{{ $cats->id }}"><a href="{{ route('pages.show', [$cats->id]) }}">{{ $cats->title }}</a>
                                    @if($cats->children)
                                        <ul role="group" class="jstree-children">
                                            @foreach($cats->children as $cat)
                                                <li aria-level="3" id="{{ $cat->id }}"><a href="{{ route('pages.show', [$cat->id]) }}">{{$cat->title}}</a></li>
                                            @endforeach
                                        </ul>
                                    @endif
                                </li>
                            @endforeach
                        </ul>
                    @endif
                </li>
            @endforeach
        </ul>
    </div>
@stop

@section('css')
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jstree/3.3.5/themes/default/style.min.css" />
@stop

@section('js')
    @parent
    <script src="//cdnjs.cloudflare.com/ajax/libs/jstree/3.3.5/jstree.min.js"></script>
    <script src="{{ asset('js/app/js/jstree.js') }}" defer></script>
@stop
