@extends('admin.layout')

@section('content')
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <a href="@yield('create_url')" class="btn btn-sm btn-default btn-flat" title="Создать">
                    <i class="fa fa-fw fa-plus"></i> Создать
                </a>
            </div>
            <div class="card-body table-responsive p-0">
                @yield('table')
            </div>
        </div>
    </div>
@stop
