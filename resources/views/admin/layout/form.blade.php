@extends('admin.layout')

@section('content')
    <div class="col-md-6">
        <div class="card card-outline card-info">
            @yield('form')
        </div>
    </div>
@stop
