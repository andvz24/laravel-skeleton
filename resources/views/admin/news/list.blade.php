@extends('admin.layout.table')

@section('title', 'Новости')

@section('breadcrumbs')
    <li class="breadcrumb-item">Новости</li>
@endsection

@section('create_url')
    {{ route('news.create') }}
@endsection

@section('table')
    <table class="table table-hover table-sm">
        <thead>
        <tr>
            <th>#ID</th>
            <th>Заголовок</th>
            <th>Дата</th>
            <th>Обновлено</th>
            <th>Опубликовано</th>
        </tr>
        </thead>
        <tbody>
            @foreach ($news as $item)
                <tr>
                    <td><a href="{{ route('news.show', [$item->id]) }}">{{ $item->id }}</a></td>
                    <td><a href="{{ route('news.show', [$item->id]) }}">{{ $item->title }}</a></td>
                    <td>{{ date('d.m.Y', strtotime($item->date)) }}</td>
                    <td>{{ date('d.m.Y H:m:s', strtotime($item->updated_at)) }}</td>
                    <td>@if($item->is_published === 1)<i class="far fa-check-square"></i> @endif</td>
                </tr>
            @endforeach
        </tbody>
    </table>
    <div class="d-flex justify-content-center">
        {!! $news->links() !!}
    </div>
@stop
