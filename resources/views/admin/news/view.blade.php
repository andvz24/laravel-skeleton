@extends('admin.layout.form')

@section('title')
    Редактировать новость
@endsection

@section('breadcrumbs')
    <li class="breadcrumb-item"><a href="{{ route('news.index') }}">Новости</a></li>
    <li class="breadcrumb-item">Редактирование новости</li>
@endsection

@section('form')
    @if (Session::has('success'))
        <div class="alert alert-success" role="alert">
            {{ Session::get('success') }}
        </div>
    @endif
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <form role="form"
          action="@if($news->id){{ route('news.update', $news->id) }}@else{{ route('news.store') }}@endif"
          method="POST">
            @if($news->id)
                @method('put')
            @endif
        @csrf
        <div class="card-body">
            <div class="form-group">
                <label for="title">Заголовок</label>
                <input type="text" class="form-control js-transliterate-source" id="title" name="title" placeholder="Заголовок" value="{{ $news->title }}">
            </div>
            <div class="form-group">
                <label for="alias">Алиас</label>
                <a href="#" id="js-transliterate-run" class="float-right" title="Сгенерировать алиас">Сгенерировать</a>
                <input type="text" class="form-control js-transliterate-destination" id="alias" name="alias" placeholder="Алиас" value="{{ $news->alias }}">
            </div>
            <div class="form-group">
                <label for="date">Дата</label>
                <input type="text" class="js-datepicker form-control" id="date" name="date" placeholder="Дата" value="{{ date('d.m.Y', strtotime($news->date)) }}">
            </div>
            <div class="form-group">
                <label for="anonce">Анонс</label>
                <textarea name="anonce" id="anonce" class="ckeditor form-control">{{ $news->anonce }}</textarea>
            </div>
            <div class="form-group">
                <label for="content">Контент</label>
                <textarea name="content" id="content" class="ckeditor form-control">{{ $news->content }}</textarea>
            </div>

            <div class="form-group">
                <label for="image">Картинка</label>
                @if($news->image)
                    <a href="{{$news->image}}" target="_blank" class="thumbnail">
                        <img src="{{$news->image}}" alt="">
                    </a>
                @endif
                <div class="input-group">
                    <input type="text" class="form-control" id="image" name="image" readonly="readonly" value="{{ $news->image }}">
                    <span class="input-group-btn">
                        <button type="button" class="btn btn-primary btn-flat js-open-elfinder" title="Выбрать файл">
                            <i class="far fa-save"></i>
                        </button>
                        <button type="button" class="btn btn-warning btn-flat js-elfinder-file-clear" title="Удалить файл">
                            <i class="far fa-trash-alt"></i>
                        </button>
                    </span>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label required">Опубликовано</label>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="is_published" value="1" @if($news->is_published === 1) checked @endif>
                    <label class="form-check-label">Да</label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="is_published" value="0" @if($news->is_published  === 0) checked @endif>
                    <label class="form-check-label">Нет</label>
                </div>
            </div>
        </div>

        <div class="card-footer">
            <button type="submit" class="btn btn-sm btn-success">Сохранить</button>
            @if($news->id)
                <a href="{{ route('news.create') }}" type="button" class="btn btn-sm btn-primary">Создать</a>
            @endif
        </div>
    </form>
    @if($news->id)
        <form class="form" method="POST" action="{{ route('news.destroy', $news->id) }}">
            @csrf
            @method('DELETE')
            <div class="card-body">
                <button type="submit" class="btn btn-sm btn-danger float-right">Удалить</button>
            </div>
        </form>
    @endif
@stop

@section('css')
    @parent
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker3.min.css">
@stop

@section('js')
    @parent

    <script src="{{ asset('js/app/libs/ckeditor/ckeditor.js') }}"></script>
    <script src="{{ asset('js/app/js/ckeditor.js') }}"></script>
    <script src="{{ asset('js/app/js/elfinder-file-type.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/locales/bootstrap-datepicker.ru.min.js"></script>
    <script src="{{ asset('js/app/js/datapicker.js') }}"></script>
    <script src="{{ asset('js/app/js/transliterate.js') }}"></script>
@stop


