@extends('admin.layout.table')

@section('title', 'Обратная связь')

@section('breadcrumbs')
    <li class="breadcrumb-item">Обратная связь</li>
@endsection

@section('create_url')
    {{ route('feedbacks.create') }}
@endsection

@section('table')
    <table class="table table-hover table-sm">
        <thead>
        <tr>
            <th>#ID</th>
            <th>ФИО</th>
            <th>Почта</th>
            <th>Телефон</th>
            <th>Дата обращения</th>
        </tr>
        </thead>
        <tbody>
            @foreach ($feedbacks as $item)
                <tr>
                    <td><a href="{{ route('feedbacks.show', [$item->id]) }}">{{ $item->id }}</a></td>
                    <td><a href="{{ route('feedbacks.show', [$item->id]) }}">{{ $item->name }}</a></td>
                    <td>{{ $item->email }}</td>
                    <td>{{ $item->phone }}</td>
                    <td>{{ date('d.m.Y H:m:s', strtotime($item->created_at)) }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
    <div class="d-flex justify-content-center">
        {!! $feedbacks->links() !!}
    </div>
@stop
