@extends('admin.layout.form')

@section('title')
    Редактировать обращение
@endsection

@section('breadcrumbs')
    <li class="breadcrumb-item"><a href="{{ route('news.index') }}">Обращения</a></li>
    <li class="breadcrumb-item">Редактирование обращение</li>
@endsection

@section('form')
    @if (Session::has('success'))
        <div class="alert alert-success" role="alert">
            {{ Session::get('success') }}
        </div>
    @endif
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <form role="form"
          action="@if($feedback->id){{ route('feedbacks.update', $feedback->id) }}@else{{ route('feedbacks.store') }}@endif"
          method="POST">
            @if($feedback->id)
                @method('put')
            @endif
        @csrf
        <div class="card-body">
            <div class="form-group">
                <label for="name">ФИО</label>
                <input type="text" class="form-control" id="name" name="name" placeholder="ФИО" value="{{ $feedback->name }}">
            </div>
            <div class="form-group">
                <label for="email">Почта</label>
                <input type="text" class="form-control" id="email" name="email" placeholder="Почта" value="{{ $feedback->email }}">
            </div>
            <div class="form-group">
                <label for="phone">Телефон</label>
                <input type="text" class="form-control" id="phone" name="phone" placeholder="Телефон" value="{{ $feedback->phone }}">
            </div>
            <div class="form-group">
                <label for="date">Дата обращения</label>
                <input type="text" class="js-datepicker form-control" id="date" name="date" placeholder="Дата" value="{{ date('d.m.Y H:m:s', strtotime($feedback->created_at)) }}">
            </div>
            <div class="form-group">
                <label for="question">Вопрос</label>
                <textarea name="question" id="question" class="form-control">{{ $feedback->question }}</textarea>
            </div>
        </div>

        <div class="card-footer">
            <button type="submit" class="btn btn-sm btn-success">Сохранить</button>
        </div>
    </form>
    @if($feedback->id)
        <form class="form" method="POST" action="{{ route('feedbacks.destroy', $feedback->id) }}">
            @csrf
            @method('DELETE')
            <div class="card-body">
                <button type="submit" class="btn btn-sm btn-danger float-right">Удалить</button>
            </div>
        </form>
    @endif
@stop

@section('css')
    @parent
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker3.min.css">
@stop

@section('js')
    @parent

    <script src="{{ asset('js/app/libs/ckeditor/ckeditor.js') }}"></script>
    <script src="{{ asset('js/app/js/ckeditor.js') }}"></script>
    <script src="{{ asset('js/app/js/elfinder-file-type.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/locales/bootstrap-datepicker.ru.min.js"></script>
    <script src="{{ asset('js/app/js/datapicker.js') }}"></script>
    <script src="{{ asset('js/app/js/transliterate.js') }}"></script>
@stop


